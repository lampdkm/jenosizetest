module.exports = function(app){
    var restaurant = require('../controllers/restaurant.controller');
    var googlemap = require('../controllers/googlemap.controller');
    var xyz = require('../controllers/xyz.controller');


    app.get('/findrestaur',restaurant.restaur);
    app.post('/findrestaur',restaurant.findrestaur);
    app.get('/map',googlemap.map);
    app.get('/xyz',xyz.findxyz);
    app.get('/', function(req, res, next){
        res.render('index', {
        });
    });
    app.get('*', function(req, res, next){
        res.status(404);
    
        res.send('404');
    });
};