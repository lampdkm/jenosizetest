var express = require('express');
const bodyParser = require('body-parser');
var path = require('path');

module.exports = function(){
    var app = express();
    app.set('views', __dirname + '/views'); // set express to look in this folder to render our view
    app.set('view engine', 'ejs'); // configure template engine
    app.use(express.static(path.join(__dirname,'/views')))
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json()); // parse form data client

    require('./router/jenosize')(app);
    return app;
};